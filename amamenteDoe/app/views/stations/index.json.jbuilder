json.array!(@stations) do |station|
  json.extract! station, :id, :name, :address, :latitude, :longitude, :cep, :phone, :fax, :station_type, :image
  json.url station_url(station, format: :json)
end
