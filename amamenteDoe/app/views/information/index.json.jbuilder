json.array!(@information) do |information|
  json.extract! information, :id, :title, :content, :date, :favorite, :url_video, :information_type, :category, :image
  json.url information_url(information, format: :json)
end
