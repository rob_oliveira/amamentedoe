class Information < ActiveRecord::Base

    has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
    validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/,
    :path => ':rails_root/public/assets/:id/:style/:basename:.:extension',
    :url => '/assets/:id/:style/:basename:.:extension'

    validates :title, presence: true
    validates :content, presence: true
    validates :date, presence: true
    validates :url_video, presence: true, :if => :is_video?
    
    validates :information_type, presence: true
    validates :category, presence: true, :numericality => { :greater_than => 0}


    def is_video?
      information_type == 2
    end

end
