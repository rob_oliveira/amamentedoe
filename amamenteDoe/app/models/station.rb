class Station < ActiveRecord::Base

	has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/,
  	:path => ':rails_root/public/assets/:id/:style/:basename:.:extension',
    :url => '/assets/:id/:style/:basename:.:extension'

    validates :name, presence: true
    validates :address, presence: true
    validates :latitude, presence: true
    validates :longitude, presence: true
    validates :cep, presence: true
    validates :phone, presence: true,
    		  :numericality => true,
              :length => { :minimum => 10, :maximum => 15 }
    validates :station_type, presence: true
    validates :image, presence: true

end
