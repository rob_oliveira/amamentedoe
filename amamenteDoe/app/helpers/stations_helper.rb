module StationsHelper

	def format_phone(num_phone)
		formatted_number = "("
		formatted_number << num_phone[0..1]
		formatted_number << ")"
		formatted_number << num_phone[2..5]
		formatted_number << "-"
		formatted_number << num_phone[6..9]
		formatted_number
	end

end
