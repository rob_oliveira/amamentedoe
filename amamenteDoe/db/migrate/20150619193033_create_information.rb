class CreateInformation < ActiveRecord::Migration
  def change
    create_table :information do |t|
      t.string :title
      t.string :content
      t.datetime :date
      t.boolean :favorite
      t.string :url_video
      t.integer :information_type
      t.integer :category
      t.string :image

      t.timestamps null: false
    end
  end
end
