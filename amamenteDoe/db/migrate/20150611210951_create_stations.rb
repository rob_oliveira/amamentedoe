class CreateStations < ActiveRecord::Migration
  def change
    create_table :stations do |t|
      t.string :name
      t.string :address
      t.string :latitude
      t.string :longitude
      t.string :cep
      t.string :phone
      t.string :fax
      t.integer :station_type
      t.string :image

      t.timestamps null: false
    end
  end
end
