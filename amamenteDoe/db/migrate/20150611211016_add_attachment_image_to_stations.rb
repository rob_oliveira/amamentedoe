class AddAttachmentImageToStations < ActiveRecord::Migration
  def self.up
    change_table :stations do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :stations, :image
  end
end
